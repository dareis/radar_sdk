#ifndef SRC_BRAM_INTERFACE_H_
#define SRC_BRAM_INTERFACE_H_
#include <stdio.h>
#include "xil_types.h"
#define ADC_BRAM_DEPTH 1024
#define ADC_BRAM_HEADER_BYTES 4
#define ADC_BRAM_TX_ID 0xC8
#define ADC_BRAM_RX_ID 0xCC
extern u32 *mBram1;
typedef struct{
	u8 id;           
	u8 cmd;          
	u8 addr_start;   
	u16 length;       
} bram_header_t;
extern bram_header_t bram_header;
#endif 
