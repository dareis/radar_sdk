#include <stdio.h>
#include <string.h>
#include <sleep.h>
#include <xgpio.h>
#include "xsysmon.h"
#include "xparameters.h"
#include "lwip/err.h"
#include "lwip/tcp.h"

#include "bram_interface.h"
#include "comm_messages.h"
#include "spi_interface.h"
#include "system_parameters.h"
#if defined (__arm__) || defined (__aarch64__)
#include "xil_printf.h"
#endif

// DR: ---- BRAM ----
#define mBram1_BASE XPAR_BRAM_ADC_AXI_BRAM_CTRL_1_S_AXI_BASEADDR
u32 *mBram1 = (u32 *) mBram1_BASE;
bram_header_t bram_rx = { 0 };
bram_header_t bram_tx = { 0 };
u32 rx_data_buffer[ADC_BRAM_DEPTH];
u32 *tx_buffer_ptr = NULL;
u8 tx_buffer8[ADC_BRAM_HEADER_BYTES];
u32 tx_buffer32[ADC_BRAM_HEADER_BYTES];
u8 *tx_buffer8_ptr;
u32 *tx_buffer32_ptr;
#define FIRST_STAGE_DEPTH 	10
#define BRAM_DEPTH 			2048
#define HEADER_LENGTH 		5
#define HEADER_BYTES 		HEADER_LENGTH*4


// DR: ---- SPI ----
u8 spi_buffer[7];
u8 *spi_buffer_ptr;
u32 SPI_BASE = XPAR_SPI_DMA_0_S00_AXI_BASEADDR;
u32 * spi_dma_ptr = (u32*) XPAR_SPI_DMA_0_S00_AXI_BASEADDR;
spi_header_t spi_header = { 0 };


// DR: ---- Sys Monitor ----
static XSysMon SysMonInst; 
#define SYSMON_DEVICE_ID 	XPAR_SYSMON_0_DEVICE_ID
int Status;
volatile u32 Value;
int gTemp;
int SysMonPolledExample(u16 SysMonDeviceId, int *Temp);
int SysMonPolledExample(u16 SysMonDeviceId, int *Temp) {
	int Status;
	
	XSysMon_Config *ConfigPtr;
	
	
	XSysMon *SysMonInstPtr = &SysMonInst;
	ConfigPtr = XSysMon_LookupConfig(SysMonDeviceId);
	if (ConfigPtr == NULL) {
		return XST_FAILURE;
	}
	XSysMon_CfgInitialize(SysMonInstPtr, ConfigPtr, ConfigPtr->BaseAddress);
	Status = XSysMon_SelfTest(SysMonInstPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	} else {
		printf("\r\n****  ADC Self Test Pass.\r\n");
	}
	return XST_SUCCESS;
}


#define PUT "put"
u16_t EthBytesReceived;

// DR: ---- PARSER -----
// DR: defines for Parser Actions
int *end_ptr;
int gParser_Clear = 1;
int gParser_Menu = 1;
int gParser_Ver = 1;
int gParser_Reg0 = 1;
int gParser_Reg1 = 1;
int gParser_Reg2 = 1;
int gParser_Reg3 = 1;
int gParser_Reg4 = 1;
int gParser_Reg5 = 1;
int gParser_Red = 1;
int gParser_Grn = 1;
int gParser_Blu = 1;
int gParser_Off = 1;
int gParser_Adc = 1;
int gParser_Temp = 1;
int gParser_ReadCmd = 1;
int gParser_PllOn = 1;
int gParser_PllOff = 1;
int gParser_End = 1;


uint32_t rx_addr_buffer;
char gParser[10][20];    
char gResponse0[25];
char gResponse1[128];     
char gResponse2[128];
int gParserValid = 0; 
char gParserAlpha[10];  // DR: buff for args in/out
char gParserBeta[10];
char gParserGamma[10];
char gResponse[51];
char str2[120];
char str3[3][9];
char OurBuffer[1400];
u32 * dataptr0;
u32 * dataptr1;
u32 * dataptr2;
u32 * dataptr3;
u32 * dataptr4;
int len = 0;
int gammaValid = 0;
int gammaNum = 0;   
u32 * gParserRead;
u32 * gReadReg = 0;
char buffer[25] = { 0 };
int read_reg = 0;
XGpio gRgb;
XGpio gReg;
XGpio gReg3;
XGpio gReg4;
XGpio gReg5;
XGpio gReg6;
int transfer_data() {
	return 0;
}
void print_app_header() {
#if (LWIP_IPV6==0)
	xil_printf("\n\r\n\r-----lwIP TCP echo server ------\n\r");
#else
	xil_printf("\n\r\n\r-----lwIPv6 TCP echo server ------\n\r");
#endif
	xil_printf("TCP packets sent to port 6001 will be echoed back\n\r");
}
void Clear(char *OurBuffer) {
	*OurBuffer = 0;
}
void Clear32(uint32_t *arg_buffer) {
	*arg_buffer = 0;
}
#define LED_CHANNEL 1

// DR: ---- GPIO ------
void RGB_Init(XGpio *InstancePtr, u32 Channel, u32 DeviceId) {
	XGpio_Config *ConfigPtr;
	ConfigPtr = XGpio_LookupConfig(DeviceId);
	XGpio_CfgInitialize(InstancePtr, ConfigPtr, ConfigPtr->BaseAddress);
	XGpio_SetDataDirection(InstancePtr, Channel, 0b000000);
}
void GPIO_Init(XGpio *InstancePtr, u32 DeviceId) {
	XGpio_Config *ConfigPtr;
	ConfigPtr = XGpio_LookupConfig(DeviceId);
	XGpio_CfgInitialize(InstancePtr, ConfigPtr, ConfigPtr->BaseAddress);
	XGpio_SetDataDirection(InstancePtr, 1, 0b000000);
	XGpio_SetDataDirection(InstancePtr, 2, 0b000000);
}
void GPIO_Init16(XGpio *InstancePtr, u32 DeviceId) {
	XGpio_Config *ConfigPtr;
	ConfigPtr = XGpio_LookupConfig(DeviceId);
	XGpio_CfgInitialize(InstancePtr, ConfigPtr, ConfigPtr->BaseAddress);
	XGpio_SetDataDirection(InstancePtr, 1, 0b0000000000000000);
}
void GPIO_Init8(XGpio *InstancePtr, u32 DeviceId) {
	XGpio_Config *ConfigPtr;
	ConfigPtr = XGpio_LookupConfig(DeviceId);
	XGpio_CfgInitialize(InstancePtr, ConfigPtr, ConfigPtr->BaseAddress);
	XGpio_SetDataDirection(InstancePtr, 1, 0b00000000);
}
u32 read_gpio(XGpio *InstancePtr, int channel) {
	u32 readValue;
	readValue = XGpio_DiscreteRead(InstancePtr, channel);
	readValue = XGpio_DiscreteRead(InstancePtr, channel);
	return readValue;
}
void write_dpA(XGpio *InstancePtr, int value) {
	printf("dpA write of 0x%x \r\n", value);
	XGpio_DiscreteWrite(InstancePtr, 1, value);
}
void write_dpB(XGpio *InstancePtr, int value) {
	printf("dpB write of %d \r\n", value);
	XGpio_DiscreteWrite(InstancePtr, 2, value);
}
void write_dio(XGpio *InstancePtr, int value) {
	printf("dio write of 0x%x \r\n", value);
	XGpio_DiscreteWrite(InstancePtr, 1, value);
}
void write_jA(XGpio *InstancePtr, u8 value) {
	printf("jA write of %d \r\n", value);
	XGpio_DiscreteWrite(InstancePtr, 1, value);
}
void write_jB(XGpio *InstancePtr, u8 value) {
	printf("jB write of %d \r\n", value);
	XGpio_DiscreteWrite(InstancePtr, 1, value);
}
void write_Aux(XGpio *InstancePtr, int value) {
	printf("Aux write of %d \r\n", value);
	int newvalue = 0x000F & value;
	XGpio_DiscreteWrite(InstancePtr, 1, newvalue);
}
void RGB_Blu(XGpio *InstancePtr) {
	XGpio_DiscreteWrite(InstancePtr, LED_CHANNEL, 0b100100);
}
void RGB_Grn(XGpio *InstancePtr) {
	XGpio_DiscreteWrite(InstancePtr, LED_CHANNEL, 0b010010);
}
void RGB_Grn1(XGpio *InstancePtr) {
	XGpio_DiscreteWrite(InstancePtr, LED_CHANNEL, 0b000010);
}
void RGB_Red(XGpio *InstancePtr) {
	XGpio_DiscreteWrite(InstancePtr, LED_CHANNEL, 0b001001);
}
void RGB_OFF(XGpio *InstancePtr) {
	XGpio_DiscreteWrite(InstancePtr, LED_CHANNEL, 0b000000);
}



// DR: primary handler of udp messages
err_t recv_callback(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err) {
	
	if (!p) {
		tcp_close(tpcb);
		tcp_recv(tpcb, NULL);
		return ERR_OK;
	}
	
	tcp_recved(tpcb, p->len);
	
	tcp_nagle_disable(tpcb);
	EthBytesReceived = p->len;
	if (EthBytesReceived == 1024) {
		usleep(10);
		tcp_recved(tpcb, p->len);
	}
	
	char OurBuffer[1400] = { 0 };
	char gParserAlpha[10] = { 0 };  
	char gParserBeta[10] = { 0 };
	char gParserGamma[10] = { 0 };
	char gResponse[51] = { 0 };
	char str2[125] = { 0 };
	for (int ii = 0; ii < 3; ii++) {
		str3[ii][50] = '\00';
	}

	// DR: (1) grab packet from eth into buffer
	memcpy(OurBuffer, (u8*) p->payload, EthBytesReceived);
	printf("\r\n");
	strcpy(str2, OurBuffer);
	if (EthBytesReceived > 0) {
		
		printf("NumBytesRecv = %d\r\n", EthBytesReceived);
	}
	int j = 0;
	int ctr = 0;
	for (int i = 0; i <= (strlen(str2)); i++) {
		if (str2[i] == ' ' || str2[i] == '\0') {
			str3[ctr][j] = '\0';
			ctr++;  
			j = 0;    
		} else {
			str3[ctr][j] = str2[i];
			j++;
		}
	}
	
	// DR: (2) parse out fields
	spi_header.id = OurBuffer[0];        
	spi_header.cmd = OurBuffer[1];        
	spi_header.addr = OurBuffer[2];        
	spi_header.length = OurBuffer[3];        
	spi_header.value[0] = OurBuffer[4];
	spi_header.value[1] = OurBuffer[5];
	spi_header.value[2] = OurBuffer[6];
	if (DEBUG_UART) {
		printf("****  API Packed Binary Command \r\n ");
		printf("Channel ID = \t0x%x \r\n", spi_header.id);
		printf("Command    = \t0x%x \r\n", spi_header.cmd);
		printf("Address    = \t0x%x \r\n", spi_header.addr);
		printf("Length     = \t0x%x \r\n", spi_header.length);
	}


	// DR: (3) handle different comm types

	if (spi_header.id == TRACE_CHANNEL_ID) {
		if (spi_header.cmd == TRIGGER_CMD) {
			u32*adc_gpio = (u32*) XPAR_ADC_GPIO_BASEADDR;
			u32 adc_value = *adc_gpio;
			*adc_gpio = adc_value | 0x00000001;
			usleep(50);
			*adc_gpio = 0x00000000;
			usleep(10);
			if (DEBUG_TRACE) {
				printf("Triggered Trace Buffer.\r\n");
				printf("Reading BRAM data back (first 15).\r\n");
				u32 local_addr  = mBram1_BASE;
				for (int jj = 0; jj < 15; jj++) {
					u32 alphaX = Xil_In32(local_addr);
					usleep(1);
					xil_printf(" 0x%x \t %llu \r\n", local_addr, alphaX);
					local_addr = local_addr + 4;
				}
			}
		} else if (spi_header.cmd == TRACE_TESTMODE) {
			u32*adc_gpio = (u32*) XPAR_ADC_GPIO_BASEADDR;
			u32 adc_value = *adc_gpio;
			*adc_gpio = adc_value | 0x00000002;
			usleep(50);
			*adc_gpio = 0x00000000;
			if (DEBUG_TRACE) {
				printf("Enabled test mode for ADC.\r\n");
			}
		}
	}
	
	
	if (spi_header.id == BRAM1_CHANNEL_ID) {
		if (spi_header.cmd == READ_CMD) {
			if (DEBUG_BRAM) {
				printf("****  Block RAM 1 - Read.\r\n ");
				printf("Address = \t%d \r\n", spi_header.addr);
				printf("Length  = \t%d \r\n", spi_header.length);
			}
			tx_buffer8[0] = BRAM1_RESPONSE_CHANNEL_ID;
			tx_buffer8[1] = READ_CMD;
			tx_buffer8[2] = spi_header.addr;
			tx_buffer8[3] = spi_header.length;
			*tx_buffer8_ptr = tx_buffer8[0];
			
			err = tcp_write(tpcb, tx_buffer8_ptr, 4, 1);
			
			
			for (int kk = 0; kk < 4; kk++) {
				if (DEBUG_BRAM) {
					printf("sending header[%d] = 0x%x\r\n", kk, tx_buffer8[kk]);
				}
			}
			
			
			mBram1 = (u32*)mBram1_BASE;
			usleep(1);
			u16 txlen = 4 * spi_header.length;
			err = tcp_write(tpcb, mBram1, txlen, 1);
			mBram1 = (u32*)mBram1_BASE;
			for (int kk = 0; kk < spi_header.length; kk++) {
				if (DEBUG_BRAM) {
					printf("sending data[%d] = 0x%x\r\n", kk,
							(unsigned int) *mBram1);
					++mBram1;
				}
			}
		} else if (spi_header.cmd == WRITE_CMD) {
			if (DEBUG_BRAM_WRITE) {
				printf(
						"****  BRAM1 Write Command Recv'd. (Debug Enabled.)\r\n");
				printf("BRAM 1::Start Addr = \t%d \r\n", spi_header.addr);
				printf("BRAM 1::Length     = \t%d \r\n", spi_header.length);
			}
			mBram1 = (u32*)mBram1_BASE;
			for (int kk = 0; kk < spi_header.length; kk++) {
				*mBram1 = OurBuffer[ADC_BRAM_HEADER_BYTES + (4 * kk)]
						+ (OurBuffer[ADC_BRAM_HEADER_BYTES + 4 * kk + 1] << 8)
						+ (OurBuffer[ADC_BRAM_HEADER_BYTES + 4 * kk + 2] << 16)
						+ (OurBuffer[ADC_BRAM_HEADER_BYTES + 4 * kk + 3] << 24);
				if (DEBUG_BRAM_WRITE)
					printf("*mBram1[%d] = %lu \r\n", kk, *mBram1);
				mBram1++;
			}
			
			
			
			if (DEBUG_BRAM_WRITE) {
				mBram1 = (u32*)mBram1_BASE;
				
				printf(
						"\r\n***** Reading back from BRAM. (Debug enabled.)\r\n");
				for (int jj = spi_header.addr;
						jj < (spi_header.addr + spi_header.length); jj++) {
					xil_printf(" mBram1[%d]=%llu \r\n", jj, *mBram1);
					++mBram1;
				}
			}
		}
	}
	
	
	
	if (spi_header.id == SPI_PLL_CHANNEL_ID) {
		if (spi_header.cmd == READ_CMD) {
			spi_dma_ptr = (u32*) SPI_BASE;
			++spi_dma_ptr;
			*spi_dma_ptr = (0x80 << 24) | (spi_header.addr << 16)
					| spi_header.length;
			spi_start(SPI_BASE, 0);
			printf("PLL commanded Read activated.\r\n");
			usleep(200);
			spi_dma_ptr = (u32*) SPI_BASE;
			++spi_dma_ptr;
			++spi_dma_ptr;
			++spi_dma_ptr;
			u32 * pll_read_value = (u32*) spi_dma_ptr;
			spi_buffer[0] = SPI_PLL_RESPONSE_ID;
			spi_buffer[1] = READ_CMD;
			spi_buffer[2] = spi_header.addr;
			spi_buffer[3] = (u8) spi_header.length;
			spi_buffer[4] = (u8) *pll_read_value;
			spi_buffer[5] = (u8) (*pll_read_value >> 8);
			spi_buffer[6] = (u8) (*pll_read_value >> 16);
			if (DEBUG_UART) {
				printf("\r\n");
				printf("****  Response SPI_PLL Packet \r\n");
				printf("Channel ID = \t0x%x \r\n", spi_buffer[0]);
				printf("Command    = \t0x%x \r\n", spi_buffer[1]);
				printf("Address    = \t0x%x \r\n", spi_buffer[2]);
				printf("Length     = \t0x%x \r\n", spi_buffer[3]);
				printf("Paylod     = \t0x%x \r\n", *pll_read_value);
			}
			*spi_buffer_ptr = spi_buffer[0];
			if (spi_header.length == 1) {
				err = tcp_write(tpcb, &spi_buffer[0], 5, 1);
				if (DEBUG_UART) {
					printf("value[0] = \t0x%x \r\n", spi_buffer[4]);
				}
			} else if (spi_header.length == 2) {
				err = tcp_write(tpcb, &spi_buffer[0], 6, 1);
				if (DEBUG_UART) {
					printf("value[0] = \t0x%x \r\n", spi_buffer[4]);
					printf("value[1] = \t0x%x \r\n", spi_buffer[5]);
				}
			} else if (spi_header.length == 3) {
				err = tcp_write(tpcb, &spi_buffer[0], 7, 1);
				if (DEBUG_UART) {
					printf("value[0] = \t0x%x \r\n", spi_buffer[4]);
					printf("value[1] = \t0x%x \r\n", spi_buffer[5]);
					printf("value[2] = \t0x%x \r\n", spi_buffer[6]);
				}
			}
			if (DEBUG_UART)
				printf("PLL commanded read values sent back.\r\n");
		} else if (spi_header.cmd == WRITE_CMD) {
			spi_dma_ptr = (u32*) SPI_BASE;
			printf("0x%x \t\t 0x%x \r\n", spi_dma_ptr, *spi_dma_ptr);
			++spi_dma_ptr;
			*spi_dma_ptr = (0x00 << 24) | (spi_header.addr << 16)
					| spi_header.length;
			if (DEBUG_UART) {
				printf("0x%x \t\t 0x%x \r\n", spi_dma_ptr, *spi_dma_ptr);
			}
			++spi_dma_ptr;
			*spi_dma_ptr = (spi_header.value[2] << 16)
					| spi_header.value[1] << 8 | spi_header.value[0];
			if (DEBUG_UART) {
				printf("0x%x \t\t 0x%x \r\n", spi_dma_ptr, *spi_dma_ptr);
			}
			spi_start(SPI_BASE, 0);
			if (DEBUG_UART)
				printf("PLL commanded Write activated.\r\n");
		}
	}
	
	
	
	if (spi_header.id == SPI_ADC_CHANNEL_ID) {
		if (spi_header.cmd == READ_CMD) {
			spi_dma_ptr = (u32*) SPI_BASE;
			++spi_dma_ptr;
			*spi_dma_ptr = (0x80 << 24) | (spi_header.addr << 16)
					| spi_header.length;
			spi_start(SPI_BASE, 1);
			printf("SPI ADC read activated.\r\n");
			usleep(200);
			spi_dma_ptr = (u32*) SPI_BASE;
			++spi_dma_ptr;
			++spi_dma_ptr;
			++spi_dma_ptr;
			u32 * adc_read_value = (u32*) spi_dma_ptr;
			spi_buffer[0] = SPI_ADC_RESPONSE_ID;
			spi_buffer[1] = READ_CMD;
			spi_buffer[2] = spi_header.addr;
			spi_buffer[3] = (u8) spi_header.length;
			spi_buffer[4] = (u8) *adc_read_value;
			spi_buffer[5] = (u8) (*adc_read_value >> 8);
			spi_buffer[6] = (u8) (*adc_read_value >> 16);
			if (DEBUG_UART) {
				printf("\r\n");
				printf("****  Response SPI_ADC Packet \r\n");
				printf("Channel ID = \t0x%x \r\n", spi_buffer[0]);
				printf("Command    = \t0x%x \r\n", spi_buffer[1]);
				printf("Address    = \t0x%x \r\n", spi_buffer[2]);
				printf("Length     = \t0x%x \r\n", spi_buffer[3]);
				printf("Paylod     = \t0x%x \r\n", spi_buffer[4]);
			}
			*spi_buffer_ptr = spi_buffer[0];
			if (spi_header.length == 1)
				err = tcp_write(tpcb, &spi_buffer[0], 5, 1);
			if (DEBUG_UART)
				printf("SPI ADC commanded read values sent back.\r\n");
		} else if (spi_header.cmd == WRITE_CMD) {
			spi_dma_ptr = (u32*) SPI_BASE;
			printf("0x%x \t\t 0x%x \r\n", spi_dma_ptr, *spi_dma_ptr);
			++spi_dma_ptr;
			*spi_dma_ptr = (0x00 << 24) | (spi_header.addr << 16)
					| spi_header.length;
			if (DEBUG_UART) {
				printf("0x%x \t\t 0x%x \r\n", spi_dma_ptr, *spi_dma_ptr);
			}
			++spi_dma_ptr;
			*spi_dma_ptr = (spi_header.value[2] << 16)
					| spi_header.value[1] << 8 | spi_header.value[0];
			if (DEBUG_UART) {
				printf("0x%x \t\t 0x%x \r\n", spi_dma_ptr, *spi_dma_ptr);
			}
			spi_start(SPI_BASE, 1);
			if (DEBUG_UART)
				printf("PLL commanded Write activated.\r\n");
		}
	}


	// DR:  ?? perhaps checking for extra args?
	sprintf(gParserAlpha, "%s", str3[0]);
	sprintf(gParserBeta, "%s", str3[1]);
	sprintf(gParserGamma, "%s", str3[2]);
	len = strlen(gParserGamma);
	u32* dataptr0 = (u32 *) gParserGamma;
	if (strstr(gParserGamma, "0x") && (len > 6)) {
		gammaValid = 1;
		sscanf(gParserGamma, "%x", &gammaNum);
	} else {
		gammaValid = 0;
		sprintf(gResponse, "%s", "Not valid argument.");
		gammaNum = 0;
	}
	
	
	// DR: (4) set action flags
	gParser_Clear = strncmp(OurBuffer, CLEAR, 5);
	gParser_Menu = !strncmp(OurBuffer, GET_OPT, 7)
			|| !strncmp(OurBuffer, HELP, 4);
	gParser_Ver = strncmp(OurBuffer, GET_VER, 7);
	gParser_Reg0 = strncmp(OurBuffer, PUT_REG0, 8);
	gParser_Reg1 = strncmp(OurBuffer, PUT_REG1, 8);
	gParser_Reg2 = strncmp(OurBuffer, PUT_REG2, 8);
	gParser_Reg3 = strncmp(OurBuffer, PUT_REG3, 8);
	gParser_Reg4 = strncmp(OurBuffer, PUT_REG4, 8);
	gParser_Reg5 = strncmp(OurBuffer, PUT_REG5, 8);
	gParser_Red = strncmp(OurBuffer, PUT_RED, 7);
	gParser_Grn = strncmp(OurBuffer, PUT_GRN, 7);
	gParser_Blu = strncmp(OurBuffer, PUT_BLU, 7);
	gParser_Off = strncmp(OurBuffer, PUT_OFF, 7);
	gParser_Adc = strncmp(OurBuffer, GET_ADC, 7);
	gParser_Temp = strncmp(OurBuffer, GET_TEMP, 8);
	gParser_ReadCmd = strncmp(OurBuffer, GET_REGS, 8);
	gParser_PllOn = strncmp(OurBuffer, PLL_ON, 6);
	gParser_PllOff = strncmp(OurBuffer, PLL_OFF, 7);
	gParser_Off = strncmp(OurBuffer, OFF, 3);

	// DR: (5) take actions
	if (!gParser_Clear) {
		err = tcp_write(tpcb, SEND_CLEAR, sizeof(SEND_CLEAR), 1);
		err = tcp_write(tpcb, SEND_HOME, sizeof(SEND_HOME), 1);
	} else if (!gParser_Red) {
		RGB_Red(&gRgb);
	} else if (!gParser_Grn) {
		RGB_Grn(&gRgb);
	} else if (!gParser_Blu) {
		RGB_Blu(&gRgb);
	} else if (!gParser_Off) {
		RGB_OFF(&gRgb);
	} else if (gParser_Menu) {
		// DR: pass over available socket commands
		err = tcp_write(tpcb, Menu_1, sizeof(Menu_1), 1);
		err = tcp_write(tpcb, Menu_3, sizeof(Menu_3), 1);
		err = tcp_write(tpcb, Menu_6, sizeof(Menu_6), 1);
		err = tcp_write(tpcb, Menu_7, sizeof(Menu_7), 1);
		err = tcp_write(tpcb, Menu_8, sizeof(Menu_8), 1);
		err = tcp_write(tpcb, Menu_8a, sizeof(Menu_8a), 1);
		err = tcp_write(tpcb, Menu_10, sizeof(Menu_10), 1);
		err = tcp_write(tpcb, Menu_10a, sizeof(Menu_10a), 1);
		err = tcp_write(tpcb, Menu_10b, sizeof(Menu_10b), 1);
		err = tcp_write(tpcb, Menu_10c, sizeof(Menu_10c), 1);
		err = tcp_write(tpcb, Menu_10d, sizeof(Menu_10d), 1);
		err = tcp_write(tpcb, Menu_10e, sizeof(Menu_10e), 1);
		err = tcp_write(tpcb, Menu_10f, sizeof(Menu_10f), 1);
		err = tcp_write(tpcb, Menu_10g, sizeof(Menu_10g), 1);
		err = tcp_write(tpcb, Menu_10h, sizeof(Menu_10h), 1);
		err = tcp_write(tpcb, Menu_10i, sizeof(Menu_10i), 1);
		err = tcp_write(tpcb, Menu_10j, sizeof(Menu_10j), 1);
		err = tcp_write(tpcb, Menu_10k, sizeof(Menu_10k), 1);
		err = tcp_write(tpcb, Menu_10l, sizeof(Menu_10l), 1);
		err = tcp_write(tpcb, Menu_10m, sizeof(Menu_10m), 1);
		err = tcp_write(tpcb, Menu_10n, sizeof(Menu_10n), 1);
	} else if (!gParser_Ver) {
		// DR: pass version details
		err = tcp_write(tpcb, VERSION_DATE, sizeof(VERSION_DATE), 1);
		err = tcp_write(tpcb, VERSION_PLATFORM, sizeof(VERSION_PLATFORM), 1);
		err = tcp_write(tpcb, VERSION_VIVADO, sizeof(VERSION_VIVADO), 1);
		err = tcp_write(tpcb, VERSION_REL, sizeof(VERSION_REL), 1);
		err = tcp_write(tpcb, VERSION_SW, sizeof(VERSION_SW), 1);
		err = tcp_write(tpcb, VERSION_HW, sizeof(VERSION_HW), 1);
		err = tcp_write(tpcb, VERSION_PART, sizeof(VERSION_PART), 1);
		err = tcp_write(tpcb, VERSION_FAMILY, sizeof(VERSION_FAMILY), 1);
		err = tcp_write(tpcb, VERSION_DEVBD, sizeof(VERSION_DEVBD), 1);
	}
	// DR: read GPIO values
	else if (!gParser_ReadCmd) {
		read_reg = XGpio_DiscreteRead(&gReg, 1);
		sprintf(buffer, "reg0 0x%x\r\n", read_reg);
		printf("read_reg0 = %s\n", buffer);
		err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		read_reg = XGpio_DiscreteRead(&gReg, 2);
		buffer[0] = 0;
		sprintf(buffer, "reg1 0x%x\r\n", read_reg);
		printf("read_reg1 = %s\n", buffer);
		err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		read_reg = XGpio_DiscreteRead(&gReg3, 1);
		buffer[0] = 0;
		sprintf(buffer, "reg2 0x%x\r\n", read_reg);
		printf("read_reg2 = %s\n", buffer);
		err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		read_reg = XGpio_DiscreteRead(&gReg4, 1);
		buffer[0] = 0;
		sprintf(buffer, "reg3 0x%x\r\n", read_reg);
		printf("read_reg3 = %s\n", buffer);
		err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		read_reg = XGpio_DiscreteRead(&gReg5, 1);
		buffer[0] = 0;
		sprintf(buffer, "reg4 0x%x\r\n", read_reg);
		printf("read_reg4 = %s\n", buffer);
		err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		read_reg = XGpio_DiscreteRead(&gReg6, 1);
		buffer[0] = 0;
		sprintf(buffer, "reg5 0x%x\r\n", (u8) read_reg);
		printf("read_reg5 = %s\n", buffer);
		err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
	// DR: on/off PLL
	} else if (!gParser_PllOn) {
		spi_dma_ptr = (u32*) SPI_BASE;
		++spi_dma_ptr;
		*spi_dma_ptr = 0x00020001;
		++spi_dma_ptr;
		*spi_dma_ptr = 0x00000001;
		spi_start(SPI_BASE, 1);
		printf("spi_pll_on() activated. \r\n");
	} else if (!gParser_PllOff) {
		spi_dma_ptr = (u32*) SPI_BASE;
		++spi_dma_ptr;
		*spi_dma_ptr = 0x00020001;
		++spi_dma_ptr;
		*spi_dma_ptr = 0x00000000;
		spi_start(SPI_BASE, 1);
		printf("PLL commanded OFF.\r\n");
	// DR: turn off FPGA
	} else if (!gParser_End) {
		// @TODO: shutdown FPGA
		printf("FPGA command OFF.\r\n");
	}
	// DR: set GPIO values
	else if (!gParser_Reg0) {
		err = tcp_write(tpcb, REG0, sizeof(REG0), 1);
		if (gammaValid) {
			
			write_dpA(&gReg, gammaNum);
			read_reg = XGpio_DiscreteRead(&gReg, 1);
			buffer[0] = 0;
			sprintf(buffer, "reg0 0x%x\r\n", read_reg);
			err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		} else
			err = tcp_write(tpcb, gResponse, sizeof(gResponse), 1);
	} else if (!gParser_Reg1) {
		err = tcp_write(tpcb, REG1, sizeof(REG1), 1);
		if (gammaValid) {
			
			write_dpB(&gReg, gammaNum);
			read_reg = XGpio_DiscreteRead(&gReg, 2);
			buffer[0] = 0;
			sprintf(buffer, "reg1 0x%x\r\n", read_reg);
			err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		} else
			err = tcp_write(tpcb, gResponse, sizeof(gResponse), 1);
	} else if (!gParser_Reg2) {
		err = tcp_write(tpcb, REG2, sizeof(REG2), 1);
		if (gammaValid) {
			
			write_dio(&gReg3, gammaNum);
			read_reg = XGpio_DiscreteRead(&gReg3, 1);
			buffer[0] = 0;
			sprintf(buffer, "reg2 0x%x\r\n", read_reg);
			err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		} else
			err = tcp_write(tpcb, gResponse, sizeof(gResponse), 1);
	} else if (!gParser_Reg3) {
		err = tcp_write(tpcb, REG3, sizeof(REG3), 1);
		if (gammaValid) {
			
			write_jA(&gReg4, gammaNum);
			read_reg = XGpio_DiscreteRead(&gReg4, 1);
			buffer[0] = 0;
			sprintf(buffer, "reg3 0x%x\r\n", read_reg);
			err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		} else
			err = tcp_write(tpcb, gResponse, sizeof(gResponse), 1);
	} else if (!gParser_Reg4) {
		err = tcp_write(tpcb, REG4, sizeof(REG4), 1);
		if (gammaValid) {
			
			write_jB(&gReg5, gammaNum);
			read_reg = XGpio_DiscreteRead(&gReg5, 1);
			buffer[0] = 0;
			sprintf(buffer, "reg4 0x%x\r\n", read_reg);
			err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		} else
			err = tcp_write(tpcb, gResponse, sizeof(gResponse), 1);
	} else if (!gParser_Reg5) {
		err = tcp_write(tpcb, REG5, sizeof(REG5), 1);
		if (gammaValid) {
			
			write_Aux(&gReg6, gammaNum);
			read_reg = XGpio_DiscreteRead(&gReg6, 1);
			buffer[0] = 0;
			sprintf(buffer, "reg5 0x%x\r\n", read_reg);
			err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		} else
			err = tcp_write(tpcb, gResponse, sizeof(gResponse), 1);
	// DR: Readback book-keeper ADC
	} else if (!gParser_Adc) {
		err = tcp_write(tpcb, ADC, sizeof(ADC), 1);
		u16 gAdc_5_raw = XSysMon_GetAdcData(&SysMonInst, 21);
		float gAdc_5_float = XSysMon_RawToVoltage(gAdc_5_raw);
		gAdc_5_float = 1.105 * gAdc_5_float;
		buffer[0] = 0;
		sprintf(buffer, "adc05 %.2f\r\n", gAdc_5_float);
		err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		printf("A5 = %d (raw) %.2f Volts \n\r", gAdc_5_raw, gAdc_5_float);
	// DR: Read back IC temp
	} else if (!gParser_Temp) {
		err = tcp_write(tpcb, TEMP, sizeof(TEMP), 1);
		u16 TempData = XSysMon_GetAdcData(&SysMonInst, XSM_CH_TEMP);
		float TempF = XSysMon_RawToTemperature(TempData);
		buffer[0] = 0;
		sprintf(buffer, "temp %.2f\r\n", TempF);
		err = tcp_write(tpcb, buffer, sizeof(buffer), 1);
		printf("Temp = %.2f   \n\r", TempF);
	}
	if (tcp_sndbuf(tpcb) > 0) {
		printf("\r\n***Entering tcp_sndbuf....\r\n");
		err = tcp_write(tpcb, "\r\n", 2, 1);
		
		
	} else
		xil_printf("no space in tcp_sndbuf\n\r");

	// DR: (6) clear out locals
	*mBram1 = 0;
	mBram1 = (u32*) mBram1_BASE;
	gParser_Menu = 1;
	gParser_Ver = 1;
	gParser_Reg0 = 1;
	gParser_Reg1 = 1;
	gParser_Reg2 = 1;
	gParser_Reg3 = 1;
	gParser_Reg4 = 1;
	gParser_ReadCmd = 1;
	gParser_Adc = 1;
	gParser_PllOn = 1;
	gParser_PllOff = 1;
	*dataptr0 = 0;
	*dataptr1 = 0;
	*dataptr2 = 0;
	*dataptr3 = 0;
	*dataptr4 = 0;
	*tx_buffer32_ptr = 0;
	*tx_buffer8_ptr = 0;
	*spi_dma_ptr = 0;
	spi_header.id = 0;        
	spi_header.cmd = 0;        
	spi_header.addr = 0;        
	spi_header.length = 0;
	spi_header.value[0] = 0;
	spi_header.value[1] = 0;
	spi_header.value[2] = 0;
	*spi_buffer = 0;
	*spi_buffer_ptr = 0;
	len = 0;
	pbuf_free(p);
	return ERR_OK;
}
err_t accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err) {
	static int connection = 1;
	tcp_recv(newpcb, recv_callback);
	tcp_arg(newpcb, (void*) (UINTPTR) connection);
	connection++;
	return ERR_OK;
}

// DR: method used in main()
int start_application() {
	struct tcp_pcb *pcb;
	err_t err;
	unsigned port = 7;
	printf("\r\n****  start_application() entered . . . \r\n");

	// DR: init modules
	RGB_Init(&gRgb, LED_CHANNEL, XPAR_AXI_GPIO_1_DEVICE_ID);
	GPIO_Init(&gReg, XPAR_AXI_GPIO_0_DEVICE_ID);
	GPIO_Init16(&gReg3, XPAR_AXI_GPIO_3_DEVICE_ID);
	usleep(100);
	GPIO_Init8(&gReg4, XPAR_AXI_GPIO_4_DEVICE_ID);
	usleep(100);
	GPIO_Init8(&gReg5, XPAR_AXI_GPIO_5_DEVICE_ID);
	GPIO_Init8(&gReg6, XPAR_AXI_GPIO_6_DEVICE_ID);

	printf("\r\n****  Initializing RGB leds (Red,Green,Blue blink.)\r\n");
	for (int kk = 0; kk < 5; kk++) {
		RGB_Red(&gRgb);
		usleep(200000);
		RGB_OFF(&gRgb);
	}
	for (int kk = 0; kk < 5; kk++) {
		RGB_Grn(&gRgb);
		usleep(200000);
		RGB_OFF(&gRgb);
	}
	for (int kk = 0; kk < 5; kk++) {
		RGB_Blu(&gRgb);
		usleep(200000);
		RGB_OFF(&gRgb);
	}

	// DR: clear out BRAM
	mBram1 = (u32*)mBram1_BASE;
	for (int kk = 0; kk < 1024; kk++) {
		mBram1[kk] = 0;
	}
	printf("\r\n****  Initializing BRAM to all 0's.\r\n");

	printf("%s", VERSION_DATE);
	printf("%s", VERSION_REL);
	printf("%s", VERSION_HW);
	printf("%s", VERSION_SW);
	int Status;
	int Temp;
	Status = SysMonPolledExample(SYSMON_DEVICE_ID, &Temp);
	if (Status != XST_SUCCESS) {
		xil_printf("Sysmon polled Example Failed\r\n");
		return XST_FAILURE;
	}
	
	// DR: set up TCP connection
	pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
	if (!pcb) {
		xil_printf("Error creating PCB. Out of Memory\n\r");
		return -1;
	}
	
	err = tcp_bind(pcb, IP_ANY_TYPE, port);
	if (err != ERR_OK) {
		xil_printf("Unable to bind to port %d: err = %d\n\r", port, err);
		return -2;
	}
	
	tcp_arg(pcb, NULL);
	
	pcb = tcp_listen(pcb);
	if (!pcb) {
		xil_printf("Out of memory while tcp_listen\n\r");
		return -3;
	}
	
	// DR: link tcp callback
	tcp_accept(pcb, accept_callback);
	xil_printf("TCP echo server started @ port %d\n\r", port);
	return 0;
}
