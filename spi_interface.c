#include "spi_interface.h"

#include <stdio.h>
#include <sleep.h>
#include "xil_types.h"
void spi_start(u32 BASE_ADDR, u8 channel){
	spi_dma_ptr = (u32*)BASE_ADDR;
	*spi_dma_ptr = 0x00000001 | (channel << 24);
	usleep(1);
	spi_dma_ptr = (u32*)BASE_ADDR;
	*spi_dma_ptr = 0x00000000 | (channel << 24);
}
