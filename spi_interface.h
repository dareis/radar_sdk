#ifndef SRC_SPI_INTERFACE_H_
#define SRC_SPI_INTERFACE_H_
#include <stdio.h>
#include "xil_types.h"
extern u32* spi_dma_ptr;
#define READ_CMD  0x00
#define WRITE_CMD 0x01
#define SPI_PLL_CHANNEL_ID  0xAA
#define SPI_PLL_RESPONSE_ID 0xA8
#define SPI_ADC_CHANNEL_ID  0xBB
#define SPI_ADC_RESPONSE_ID 0xB8
#define BRAM1_CHANNEL_ID    0xCC
#define BRAM1_RESPONSE_CHANNEL_ID 0xC8
#define TRACE_CHANNEL_ID    0xDD
#define TRACE_TESTMODE      0x0F
#define TRIGGER_CMD         0x02
#define ARM_CMD             0x01
typedef struct {
	u8  id;                 
	u8  cmd;                
	u8  length;             
	u8  addr;               
	u8  value[3];           
} spi_header_t;
void spi_start(u32 BASE_ADDR, u8 channel);
#endif 
